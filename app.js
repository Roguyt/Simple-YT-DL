/**
 * Simple-YT-DL
 * TODO: Description
 * Author: Roguyt
 */

/**
 * Modules dependencies
 */

const ytdl          = require('youtube-dl');
const SpotifyWebApi = require('spotify-web-api-node');
const inquirer      = require('inquirer');
const _             = require('lodash');

const fs = require('fs');

/**
 * Additional dependencies
 */

const config = require('./config/config');

/**
 * Spotify API Setup
 */

const spotifyApi = new SpotifyWebApi(config.spotifyApi);

spotifyApi
    .clientCredentialsGrant()
    .then(function(data) {
        spotifyApi.setAccessToken(data.body['access_token']);

        main();
    }, function(err) {
        console.log('Something went wrong when retrieving an access token', err.message);
    })
;

/**
 * Utilities
 */

function isWhat(url) {
    if (url.match(/^https?:\/\/(?:open|play)\.spotify\.com\/track\/[\w\d]+$/i)) {
        return {
            type: 'track',
            id: url.match(/\/(album|track|artist)\/(.*)/i)[2]
        };
    } else {
        if (url.match(/^https?:\/\/(?:open|play)\.spotify\.com\/album\/[\w\d]+$/i)) {
            return {
                type: 'album',
                id: url.match(/\/(album|track|artist)\/(.*)/i)[2]
            };
        } else {
            if (url.match(/^https?:\/\/(?:open|play)\.spotify\.com\/artist\/[\w\d]+$/i)) {
                return {
                    type: 'artist',
                    id: url.match(/\/(album|track|artist)\/(.*)/i)[2]
                };
            } else {
                return null;
            }
        }
    }
}

/**
 * Functions
 */

function youtubeLink() {
    inquirer
        .prompt([{
            type: 'input',
            name: 'url',
            message: 'Url YouTube ?'
        }])
        .then(answers => {
            url = answers.url;

            url = 'https://www.youtube.com/watch?v=0PIePjwyZ8M'; // Album
            //url = 'https://www.youtube.com/watch?v=NdtBkN3vrpQ'; // Not Spotify
            //url = 'https://www.youtube.com/watch?v=iHUWUXlr-CM'; // Artist

            if (url.match(/^(https?\:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/)) {
                ytdl.getInfo(url, (err, info) => {
                    if (err) throw err;

                    let infos = info;

                    console.log(`You're going to download the music ${infos.title} from the channel ${infos.uploader_id}`);

                    spotifyLinkAvailable();
                });
            } else {
                youtubeLink();
            }
        })
        .catch(err => {
            console.log(err);
        })
    ;
}

function spotifyLinkAvailable() {
    inquirer
        .prompt([{
            type: 'confirm',
            name: 'spotifyAvailable',
            message: 'Do you have a spotify link to the track'
        }])
        .then(newAnswers => {
            if (newAnswers.spotifyAvailable) {
                spotifyLink();
            } else {
                // todo (Just download and fuck it or edit ?)
            }
        })
    ;
}

function spotifyLink() {
    inquirer
        .prompt([{
            type: 'input',
            name: 'spotifyUrl',
            message: 'Link to the track on Spotify'
        }])
        .then(answers => {
            //answers.spotifyUrl = 'https://open.spotify.com/track/5C5JdQhZiFzdnfm9wYjLmm';
            answers.spotifyUrl = 'https://open.spotify.com/track/7p1bMlkJwkRNtUbIO7GHjt';
            //answers.spotifyUrl = 'https://open.spotify.com/track/67WMyaBOp5pwWeBXLIJ0ra';

            if (isWhat(answers.spotifyUrl) !== null && isWhat(answers.spotifyUrl).type === 'track') {
                let spotifyId = isWhat(answers.spotifyUrl).id;

                spotifyApi
                    .getTrack(spotifyId)
                    .then(data => {
                        spotifyApi
                            .getAlbum(data.body.album.id)
                            .then(albumData => {
                                let musicDatas = {
                                    url: url,
                                    title: data.body.name,
                                    album: data.body.album.name,
                                    year: new Date(albumData.body.release_date).getFullYear(),
                                    pos: data.body.track_number,
                                    disc_number: data.body.disc_number,
                                    disc_amount: data.body.disc_number,
                                    copyright: albumData.body.copyrights[0].text.replace(/\d+ +/, ''),
                                    albumArtists: data.body.album.artists,
                                    contributingArtists: data.body.artists,
                                    cover: data.body.album.images[0].url
                                };

                                inquirer
                                    .prompt([{
                                        type: 'confirm',
                                        name: 'datasOk',
                                        message: 'Are you OK with the datas collected ?'
                                    }])
                                    .then(answers => {
                                        if (answers.datasOk) {
                                            downloadFile(musicDatas);
                                        } else {
                                            inquirer
                                                .prompt([{
                                                    type: 'editor',
                                                    name: 'editDatas',
                                                    message: 'Use your text editor to fix the data',
                                                    default: JSON.stringify(musicDatas)
                                                }])
                                                .then(answers => {
                                                    musicDatas = JSON.parse(answers.editDatas);
                                                    downloadFile(musicDatas);
                                                })
                                            ;
                                        }
                                    })
                                ;
                            })
                            .catch(err => {
                                throw err;
                            })
                        ;
                    })
                    .catch(err => {
                        throw err;
                    })
                ;
            } else {
                spotifyLink();
            }
        })
    ;
}

function downloadFile(metadatas) {
    let music = ytdl.exec(metadatas.url, ['--extract-audio', '--audio-format',  'mp3', '--audio-quality',  '0'],  {cwd: __dirname}, (err, output) => {
        if (err) throw err;

        console.log(output.join('\n'));
        console.log(metadatas);
    });
}

/**
 * Launching user interaction
 */

function main() {
    let url;
    youtubeLink();
}

/*
                    let spotifyUrl = null;

                    if ((infos.description.match(/spotify(.*)/i)) && ((infos.description.match(/spotify(.*)/i)[1]).match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/))) {
                        spotifyUrl = (infos.description.match(/spotify(.*)/i)[1]).match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)[0];
                    }

                    if (spotifyUrl !== null) {
                        spotifyUrl = spotifyUrl.replace(/\?.*//*i, '');
                        let spotifyInput = isWhat(spotifyUrl);

                        spotifyApi
                            .getAlbum(spotifyInput.id)
                            .then(data => {
                                console.log(data);
                            })
                            .catch(err => {
                                console.log(err);
                            })
                        ;
                    } else {

                    }*/