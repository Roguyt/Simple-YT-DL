/**
 * Modules dependencies
 */

const pkg = require('../package.json');

/**
 * Configuration file
 */

const config           = {};

config.env             = process.env.NODE_ENV || 'production';

config.version         = pkg.version;

/**
 * Spotify Web API
 */

config.spotifyAPi              = {};
config.spotifyAPi.clientId     = '';
config.spotifyAPi.clientSecret = '';
config.spotifyAPi.redirectUri  = '';


module.exports = config;